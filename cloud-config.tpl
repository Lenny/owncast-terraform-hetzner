#cloud-config
ssh_authorized_keys:
  - ${ssh_key}

groups:
  - caddy

users:
  - name: caddy
    gecos: Caddy web server
    primary_group: caddy
    groups: caddy
    shell: /usr/sbin/nologin
    homedir: /var/lib/caddy
  - name: owncast
    gecos: Owncast streaming server
    primary_group: owncast
    groups: owncast
    shell: /usr/sbin/nologin
    homedir: /home/owncast

write_files:
- content: |
    {
      email ${letsencrypt_email}
      #acme_ca https://acme-staging-v02.api.letsencrypt.org/directory
    }

    ${domain_name} {
      encode gzip
      reverse_proxy 127.0.0.1:8080
    }
  path: /etc/caddy/Caddyfile
- content: |
    [Unit]
    Description=Owncast
    Documentation=https://owncast.online/docs/
    After=network.target network-online.target
    Requires=network-online.target

    [Service]
    User=owncast
    Group=owncast
    Type=simple
    WorkingDirectory=/home/owncast/owncast
    ExecStart=/home/owncast/owncast/owncast
    Restart=on-failure
    RestartSec=5

    [Install]
    WantedBy=multi-user.target
  path: /etc/systemd/system/owncast.service

package_update: true

packages:
  - unzip
  - ufw

runcmd:
- ufw allow 22
- ufw allow 80
- ufw allow 443
- ufw allow 1935/tcp
- ufw allow 1935/udp
- ufw enable
- ufw -f default deny incoming
- cd /home/owncast
- curl -s https://owncast.online/install.sh | bash
- cd owncast
- ./owncast -streamkey ${owncast_streaming_key}
- chown -R owncast:owncast /home/owncast
- wget https://github.com/caddyserver/caddy/releases/download/v2.3.0/caddy_2.3.0_linux_amd64.tar.gz -O /tmp/caddy.tar.gz && tar -zxvf /tmp/caddy.tar.gz -C /usr/bin/ caddy
- wget https://raw.githubusercontent.com/caddyserver/dist/master/init/caddy.service -O /etc/systemd/system/caddy.service
- systemctl daemon-reload
- systemctl enable owncast
- systemctl start owncast
- systemctl enable caddy
- systemctl start caddy