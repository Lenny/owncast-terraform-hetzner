# owncast-terraform-hetzner

automatically spins up a hetzner cloud instance with owncast running behind a caddy server (optional auto dns binding with cloudflare)

## Usage

1. Run `terraform init` to download the required terraform modules
2. Fill `terraform.tfvars` with all your api keys and such
3. run `terraform apply`
    - make note of the stream key, you will have need that for streaming and to log into the admin ui
    - give owncast two minutes to spin up