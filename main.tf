terraform {
  required_version = ">= 0.12"
}

variable "hetzner_token" {
  description = "Hetzner API token"
}
variable "cloudflare_token" {
  description = "Cloudflare API token"
}
variable "cloudflare_zone_id" {
  description = "Cloudflare zone id to create the record in"
}
variable "cloudflare_domain" {
  description = "Your public domain"
}
variable "cloudflare_subdomain" {
  description = "Your public subdomain"
  default = "cast"
}
variable "letsencrypt_email" {
  description = "Email used to order a certificate from Letsencrypt"
}
variable "cloudflare_create_record" {
  default     = false
  description = "Whether to create a DNS record on Digitalocean"
}
variable "hetzner_region" {
  default     = "nbg1"
  description = "The hetzner region where the owncast instance will be created."
}
variable "ssh_key_file" {
  description = "Path to the SSH public key file"
}

resource "random_password" "owncast_streaming_key" {
  length           = 32
  special          = false
}

provider "hcloud" {
  token = var.hetzner_token
}

provider "cloudflare" {
  api_token = var.cloudflare_token
}

data "local_file" "ssh_key"{
  filename = pathexpand(var.ssh_key_file)
}

data "template_file" "cloud_init" {
  template = file("cloud-config.tpl")
    vars = {
      ssh_key=data.local_file.ssh_key.content,
      domain_name="${var.cloudflare_subdomain}.${var.cloudflare_domain}"
      owncast_streaming_key=random_password.owncast_streaming_key.result
      letsencrypt_email=var.letsencrypt_email
    }
}

resource "hcloud_ssh_key" "default" {
  name = "terraform ssh key"
  public_key = file(var.ssh_key_file)
}

resource "hcloud_server" "owncast" {
  location    = var.hetzner_region
  image       = "debian-10"
  name        = "owncast"
  server_type = "cpx11"
  ssh_keys    = [hcloud_ssh_key.default.id]
  user_data   = data.template_file.cloud_init.rendered
}

resource "cloudflare_record" "owncast" {
  zone_id = var.cloudflare_zone_id
  type    = "A"
  name    = var.cloudflare_subdomain
  value   = hcloud_server.owncast.ipv4_address
  # Only creates record if cloudflare_create_record is true
  count   = var.cloudflare_create_record == true ? 1 : 0
}

output "streaming_key" {
  value = random_password.owncast_streaming_key.result
}

output "instace_ip" {
  value = hcloud_server.owncast.ipv4_address
}

output "owncast_url" {
  value = "https://${var.cloudflare_subdomain}.${var.cloudflare_domain}/"
}
